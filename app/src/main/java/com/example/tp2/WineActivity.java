package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine2);
        final WineDbHelper wineDb = new WineDbHelper(this);

        Intent intent = getIntent();
        Wine wine = null;
        wine = (Wine) intent.getParcelableExtra("Wine");


        final EditText wineName = (EditText) findViewById(R.id.wineName);
        final EditText editWineRegion = (EditText) findViewById(R.id.editWineRegion);
        final EditText editLoc = (EditText) findViewById(R.id.editLoc);
        final EditText editClimate = (EditText) findViewById(R.id.editClimate);
        final EditText editPlantedArea = (EditText) findViewById(R.id.editPlantedArea);

        TextView labelWineRegion = (TextView) findViewById(R.id.labelWineRegion);
        TextView labelLoc = (TextView) findViewById(R.id.labelLoc);
        TextView labelClimate = (TextView) findViewById(R.id.labelClimate);
        TextView labelPlantedArea = (TextView) findViewById(R.id.labelPlantedArea);

        wineName.setText(wine.getTitle());
        editWineRegion.setText(wine.getRegion());
        editLoc.setText(wine.getLocalization());
        editClimate.setText(wine.getClimate());
        editPlantedArea.setText(wine.getPlantedArea());

        Button button = (Button) findViewById(R.id.button);

        final Wine finalWine = wine;
        int up=0;
        if(wine.getTitle()==" ") up=1;
        final int finalUp = up;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finalWine.setTitle(wineName.getText().toString());
                finalWine.setRegion(editWineRegion.getText().toString());
                finalWine.setLocalization(editLoc.getText().toString());
                finalWine.setClimate(editClimate.getText().toString());
                finalWine.setPlantedArea(editPlantedArea.getText().toString());

                if(finalUp == 1)
                {
                    wineDb.addWine(finalWine);
                }
                else {
                    wineDb.updateWine(finalWine);

                }
                finish();
            }
        });
    }

}
