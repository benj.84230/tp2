package com.example.tp2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import static com.example.tp2.WineDbHelper.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final WineDbHelper wineDb = new WineDbHelper(this);

        //wineDb.populate();
        final Cursor cursor = wineDb.fetchAllWines();
        final ListView vue = (ListView) findViewById(R.id.listview);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.listview, cursor, new String[] {COLUMN_NAME, COLUMN_WINE_REGION },  new int[] { R.id.list_row_title,R.id.list_row_content });
        vue.setAdapter(adapter);

        vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Cursor cursor= wineDb.fetchAllWines();
                cursor.moveToFirst();
                Wine temp = null;
                while (cursor.moveToNext()) {
                    if (cursor.getPosition() == pos) {
                        temp = cursorToWine(cursor);
                    }
                }
                Intent WineActivity = new Intent(getApplicationContext(), WineActivity.class);
                WineActivity.putExtra("Wine",temp);
                startActivity(WineActivity);

            }

        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine temp = new Wine(" "," "," "," "," ");

                Intent WineActivity = new Intent(getApplicationContext(), WineActivity.class);
                WineActivity.putExtra("Wine",temp);
                startActivity(WineActivity);
            }
        });

        vue.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_main, menu);

                // Get the list
                ListView list = (ListView)vue;

                // Get the list item position
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
                final int position = info.position;

                // Now you can do whatever.. (Example, load different menus for different items)
                //list.getItemAtPosition(position);




                menu.add("Supprimer").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        Cursor cursor= wineDb.fetchAllWines();
                        cursor.moveToFirst();
                        while (cursor.moveToNext()) {
                            if (cursor.getPosition() == position) {
                                wineDb.deleteWine(cursor);
                                return true;
                            }
                        }
                        return false;

                    }
                });
            }
        });





    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
